an event driven excercise implement message flow from rest call to data store.

## Problem Description

### Background
* A merchant can set certain rules to its product when purchase transaction occured
* The customer needs to be notified if the transaction hits postive response from the rule

### Requirement
* Come up a solution to achieve this behaviour
* Demostrate golang skills


## Solution
An event driven design is made to achieve the requirements. When a transaction message arrived
at offer service, a rule engine will perform the validation of the transaction and decide whether
or not to give notification to customer.

All rules and offers are stored in the service offer_store. A preloading/code start will happen when
offer starts, and it will cache all the offer from the store. This is done for the sake of simplify
the execercise complexcity. If there is a need, RAFT can be introduced to achieve the high availability
and scalability of the service.

```

                +---------+                +-----------+
                |         |                |           |
transaction(mq) |  offer  |get_all(http)   |offer_store|
   +------------>         <----------------+           |
 <--------------+         |                |       XXXXXXXXXXXXXX
notify(mq)      +---------+                +-------X---+        X
                                                   X            X
                                                   X  pg_db     X
                                                   X            X
                                                   XXXXXXXXXXXXXX

```

### Components
* offer_store service, written in elixir. It is an http service.
* offer service, written in go. It talks to rabbit mq and offer store service
* integretaion_test repo, written in elixir to setup input and run test examples

### Supporting components
* rabbitmq
* postgres

### How to run
* Docker is needed to demo the message flow
* Elixir and mix is need to run integeration test

* run ./setup.sh to build the executable docker container by
* in integeration_test folder, run docker-compose up -d
* then run mix espec

