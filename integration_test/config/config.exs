# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
use Mix.Config

config :integration_test, :rabbit_url, "amqp://offer:offer@localhost:8004/offer"
config :integration_test, :offer_store_url, "http://localhost:8002/offer"
config :integration_test, :offer_url, "http://localhost:8006"
