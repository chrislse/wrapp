defmodule Offer.HappyPathSpec do
  use ESpec

  alias IntegrationTest.MqPublisher

  context "transaction message is sent" do
    before do
      IntegrationTest.OfferStoreClient.add_offer(%{
        merchant_id: 1,
        description: "test offer description",
        rule: "amount>100"
      })

      {:ok, response} = IntegrationTest.OfferStoreClient.get_offer()

      offers = Poison.decode!(response.body)

      IntegrationTest.OfferClient.all_offers(offers)
    end

    it "receive client notification" do
      MqPublisher.publish(
        Poison.encode!(%{
          "merchant_id" => 1,
          "category" => "fruit",
          "amount" => 120,
          "currency" => "sek"
        }),
        "translated-transactions"
      )

      assert_receive("test offer description", 5000)
    end
  end
end
