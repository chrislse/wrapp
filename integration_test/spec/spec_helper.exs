ESpec.configure(fn config ->
  Application.ensure_started(:integration_test)

  config.before(fn tags ->
    {:shared, hello: :world, tags: tags}
  end)

  config.finally(fn _shared ->
    :ok
  end)

  Process.register(self(), ESpec)
end)
