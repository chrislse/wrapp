defmodule IntegrationTest.ConsumerCallback do
  def perform(payload, _header) do
    send(ESpec, payload)
  end
end
