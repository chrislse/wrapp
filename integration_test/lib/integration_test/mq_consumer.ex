defmodule IntegrationTest.MqConsumer do
  use GenServer
  use AMQP

  def start_link(_arg) do
    GenServer.start_link(__MODULE__, [], name: __MODULE__)
  end

  def init(_opt) do
    initialize(%{
      rabbit_url:
        Application.get_env(:integration_test, :rabbit_url, "amqp://guest:guest@localhost"),
      consumer_queue: Application.get_env(:integration_test, :consumer_queue, "offer"),
      consumer_queue_name: Application.get_env(:integration_test, :consumer_queue, "offer"),
      exchange_type: Application.get_env(:integration_test, :consumer_exchange_type, :direct),
      exchange_name: Application.get_env(:integration_test, :consumer_exchange_name, ""),
      consumer_routing_key: Application.get_env(:integration_test, :consumer_routing_key, "#")
    })
  end

  defp initialize(
         options = %{
           rabbit_url: rabbit_url,
           consumer_queue: consumer_queue,
           consumer_queue_name: consumer_queue_name,
           exchange_type: exchange_type,
           exchange_name: exchange_name,
           consumer_routing_key: consumer_routing_key
         }
       ) do
    try do
      {:ok, conn} = Connection.open(rabbit_url)

      {:ok, chan} = Channel.open(conn)
      setup_queue(chan, options)

      {:ok, _consumer_tag} =
        Basic.consume(
          chan,
          consumer_queue,
          nil,
          consumer_tag: "integration_test"
        )

      {:ok, chan}
    rescue
      e in MatcherError ->
        initialize(options)
    end
  end

  def handle_info({:basic_consume_ok, %{consumer_tag: consumer_tag}}, chan) do
    {:noreply, chan}
  end

  def handle_info({:basic_cancel, %{consumer_tag: consumer_tag}}, chan) do
    {:stop, :normal, chan}
  end

  def handle_info({:basic_cancel_ok, %{consumer_tag: consumer_tag}}, chan) do
    {:noreply, chan}
  end

  def handle_info(
        {:basic_deliver, payload, header = %{delivery_tag: tag, redelivered: redelivered}},
        chan
      ) do
    Application.get_env(:integration_test, :consumer_callback, IntegrationTest.ConsumerCallback).perform(
      payload,
      header
    )

    {:noreply, chan}
  end

  defp setup_queue(chan, %{
         consumer_queue_name: consumer_queue_name,
         exchange_type: exchange_type,
         exchange_name: exchange_name,
         consumer_routing_key: consumer_routing_key
       }) do
    {:ok, _} = Queue.declare(chan, consumer_queue_name, durable: true)

    # default exchange does not allow declare action
    if !(exchange_name == "" && exchange_type == :direct) do
      Exchange.declare(chan, exchange_name, exchange_type)
      Queue.bind(chan, consumer_queue_name, exchange_name, routing_key: consumer_routing_key)
    end
  end
end
