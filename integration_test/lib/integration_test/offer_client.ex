defmodule IntegrationTest.OfferClient do
  @offer_url Application.get_env(:integration_test, :offer_url, "http://localhost:8081/")

  def all_offers(all_offers) do
    HTTPoison.post("#{@offer_url}/all_offers", Poison.encode!(all_offers), [
      {"Content-Type", "application/json"}
    ])
  end
end
