defmodule IntegrationTest.MqPublisher do
  use GenServer
  use AMQP

  def start_link(_arg) do
    GenServer.start_link(__MODULE__, [], name: __MODULE__)
  end

  def init(_opts) do
    initialize(%{
      rabbit_url:
        Application.get_env(:integration_test, :rabbit_url, "amqp://guest:guest@localhost"),
      exchange_name: Application.get_env(:integration_test, :publisher_exchange_name, ""),
      exchange_type: Application.get_env(:integration_test, :publisher_exchange_type, :direct)
    })
  end

  defp initialize(
         options = %{
           rabbit_url: rabbit_url,
           exchange_name: exchange_name,
           exchange_type: exchange_type
         }
       ) do
    try do
      {:ok, conn} = Connection.open(rabbit_url)

      {:ok, chan} = Channel.open(conn)

      # default exchange does not allow declare action
      if !(exchange_name == "" && exchange_type == :direct) do
        Exchange.declare(chan, exchange_name, exchange_type)
      end

      {:ok, chan}
    rescue
      e in MatcherError ->
        :timer.sleep(2000)
        initialize(options)
    end
  end

  def publish(payload, routingkey) do
    GenServer.cast(__MODULE__, {:publish, payload, routingkey})
  end

  def handle_cast({:publish, payload, routingkey}, chan) do
    exchange_name = Application.get_env(:integration_test, :publisher_exchange_name, "")
    AMQP.Basic.publish(chan, exchange_name, routingkey, payload)
    {:noreply, chan}
  end
end
