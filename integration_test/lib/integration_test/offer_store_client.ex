defmodule IntegrationTest.OfferStoreClient do
  @offer_store_url Application.get_env(
                     :integration_test,
                     :offer_store_url,
                     "http://localhost:8080/offer"
                   )

  def add_offer(offer_map) do
    body =
      offer_map
      |> Enum.map(fn {key, value} ->
        "#{key}=#{value}"
      end)
      |> Enum.join("&")

    HTTPoison.post(@offer_store_url, body, [{"Content-Type", "application/json"}])
  end

  def get_offer() do
    HTTPoison.get(@offer_store_url, [{"Content-Type", "application/json"}])
  end
end
