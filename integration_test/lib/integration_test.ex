defmodule IntegrationTest do
  @moduledoc """
  Documentation for IntegrationTest.
  """

  @doc """
  Hello world.

  ## Examples

      iex> IntegrationTest.hello
      :world

  """
  def hello do
    :world
  end
end
