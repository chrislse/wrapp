defmodule OfferStore.Http.OfferHandler do
  def init(req, state) do
    method = :cowboy_req.method(req)
    has_body = :cowboy_req.has_body(req)

    request = perform(method, has_body, req)

    {:ok, request, state}
  end

  defp perform("POST", true, request) do
    {:ok, post_vals, req} = :cowboy_req.read_urlencoded_body(request)
    process_values(post_vals, req)
  end

  defp perform("POST", false, request) do
    :cowboy_req.reply(400, %{}, "Missing body.", request)
  end

  defp perform("GET", false, request) do
    :cowboy_req.reply(
      200,
      %{"content-type" => "application/json"},
      build_all_offers_json(),
      request
    )
  end

  defp perform(_, _, request) do
    :cowboy_req.reply(405, request)
  end

  defp process_values(post_vals, req) do
    description = :proplists.get_value("description", post_vals)
    merchant_id = :proplists.get_value("merchant_id", post_vals)
    rule = :proplists.get_value("rule", post_vals)
    IO.inspect(req)
    IO.inspect(post_vals)

    case save(description, merchant_id, rule) do
      true ->
        :cowboy_req.reply(
          200,
          %{
            "content-type" => "text/plain; charset=utf-8"
          },
          "true",
          req
        )

      false ->
        :cowboy_req.reply(400, %{}, "Missing parameter.", req)
    end
  end

  defp save(description, merchant_id, rule)
       when description == :undefined or merchant_id == :undefined or rule == :undefined do
    false
  end

  defp save(description, merchant_id, rule) do
    {:ok, rule} = OfferStore.Repo.insert(%OfferStore.Data.Rule{description: rule})

    OfferStore.Repo.insert(%OfferStore.Data.Offer{
      description: description,
      merchant_id: String.to_integer(merchant_id),
      rule_id: rule.id
    })

    true
  end

  defp build_all_offers_json() do
    OfferStore.Repo.all(OfferStore.Data.Offer)
    |> Enum.map(fn offer ->
      new_map = %{}
      rule = OfferStore.Repo.get_by(OfferStore.Data.Rule, id: offer.rule_id)

      new_map
      |> Map.put(:description, offer.description)
      |> Map.put(:rule, rule.description)
      |> Map.put(:merchant_id, offer.merchant_id)
      |> Map.put(:offer_id, offer.id)
    end)
    |> Poison.encode!()
  end
end
