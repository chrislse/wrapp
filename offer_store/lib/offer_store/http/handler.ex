defmodule OfferStore.Http.Handler do
  def init(req, state) do
    handle(req, state)
  end

  def handle(request, state) do
    req = :cowboy_req.reply(200, %{"content-type" => "text/html"}, build_body(request), request)
    {:ok, req, state}
  end

  def terminate(_reason, _request, _state) do
    # log some reason here
    :ok
  end

  def build_body(_request) do
    # TODO show some pretty api guide here
    """
    Here should show some dynamic pretty user guide to the api 

    """
  end
end
