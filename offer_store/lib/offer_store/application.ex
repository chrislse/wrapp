defmodule OfferStore.Application do
  use Application

  def start(_type, _args) do
    children = [
      OfferStore.Repo
    ]

    setup_db()
    http_server()
    opts = [strategy: :one_for_one, name: OfferStore.Supervisor]
    result = Supervisor.start_link(children, opts)

    result
  end

  defp setup_db() do
    spawn(fn ->
      if System.get_env("GAMECENTER") == "docker" do
        OfferStore.Tasks.ReleaseMigrate.migrate()
      end
    end)
  end

  defp http_server do
    dispatch =
      :cowboy_router.compile([
        {:_, [{'/', OfferStore.Http.Handler, []}, {'/offer', OfferStore.Http.OfferHandler, []}]}
      ])

    {:ok, _} =
      :cowboy.start_clear(
        OfferStore.Http.Listener,
        [{:port, Application.get_env(:offer_store, :http_port, 8080)}],
        %{env: %{dispatch: dispatch}}
      )
  end
end
