defmodule OfferStore.Tasks.ReleaseMigrate do
  def migrate do
    Application.ensure_all_started(:offer_store)
    path = Application.app_dir(:offer_store, "priv/repo/migrations")

    Ecto.Migrator.run(OfferStore.Repo, path, :up, all: true)
  end
end
