defmodule OfferStore.Data.Rule do
  use Ecto.Schema

  schema "rules" do
    # Defaults to type :string
    field(:description)

    timestamps()
  end
end
