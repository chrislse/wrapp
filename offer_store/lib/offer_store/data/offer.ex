defmodule OfferStore.Data.Offer do
  use Ecto.Schema

  schema "offers" do
    # Defaults to type :string
    field(:description)
    field(:merchant_id, :integer)
    field(:rule_id, :integer)
    timestamps()
  end
end
