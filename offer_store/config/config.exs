use Mix.Config

config :offer_store, ecto_repos: [OfferStore.Repo]

db_config =
  case System.get_env("GAMECENTER") do
    "docker" ->
      %{
        hostname: "${DATABASE_HOST}",
        database: "${DATABASE_NAME}",
        username: "${DATABASE_USER}",
        password: "${DATABASE_PASS}"
      }

    _ ->
      %{hostname: "localhost", database: "offer_store", username: "offer", password: "offer"}
  end

config :offer_store, OfferStore.Repo,
  adapter: Ecto.Adapters.Postgres,
  database: db_config.database,
  username: db_config.username,
  password: db_config.password,
  hostname: db_config.hostname,
  port: "5432"
