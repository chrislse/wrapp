defmodule OfferStore.Repo.Migrations.CreateRules do
  use Ecto.Migration

  def change do
    create table("rules") do
      add(:description, :string)

      timestamps()
    end
  end
end
