defmodule OfferStore.Repo.Migrations.CreateOffers do
  use Ecto.Migration

  def change do
    create table("offers") do
      add(:description, :string)
      # FIXME it can refer to merchant table
      add(:merchant_id, :integer)
      add(:rule_id, references(:rules))

      timestamps()
    end
  end
end
