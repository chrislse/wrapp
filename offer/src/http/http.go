package http

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"log"
	"strconv"

	"github.com/gin-gonic/gin"
	"offer"
)

type fn func(decoder *json.Decoder)

func Serve(port int) {
	router := gin.Default()

	router.POST("/all_offers", func(c *gin.Context) {

		defer c.Request.Body.Close()
		bodyJson, _ := ioutil.ReadAll(c.Request.Body)

		parseJsonList(string(bodyJson), offer.ParseOffer)

		c.JSON(200, gin.H{
			"status": "ok",
		})
	})

	router.Run(":" + strconv.Itoa(port))
}

func parseJsonList(offerJson string, decodeFunc fn) {
	buf := bytes.NewBufferString(offerJson)
	dec := json.NewDecoder(buf)

	_, err := dec.Token()
	if err != nil {
		log.Print(err)
	}

	for dec.More() {
		decodeFunc(dec)
	}

	_, err = dec.Token()
	if err != nil {
		log.Print(err)
	}
	log.Print("fetch all offers")
}
