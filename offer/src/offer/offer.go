package offer

import (
	"encoding/json"
	"github.com/caibirdme/yql"
	"log"

	"mq"
)

type Offer struct {
	OfferId     int `json:"offer_id"`
	Description string
	Rule        string
	MerchantId  int `json:"merchant_id"`
}

var allOffers map[int]Offer

func init() {
	allOffers = make(map[int]Offer)
}

func RunRuleEngine(transactionData map[string]interface{}) {
	log.Printf("%v", allOffers)
	for offerId, offer := range allOffers {
		result, err := yql.Match(offer.Rule, transactionData)
		log.Print(transactionData)
		if err != nil {
			log.Printf("Cannot run offer %v rule: %s, %s", offerId, offer.Rule, err)
		}

		if result {
			//TODO perform further actions by using handler
			mq.Publish(offer.Description, "offer")
			log.Printf("Found offer - %s.", offer.Description)
		}
	}
}

func ParseOffer(decoder *json.Decoder) {
	var offer Offer
	err := decoder.Decode(&offer)
	if err != nil {
		log.Printf("%s", err)
	}

	allOffers[offer.OfferId] = offer
}
