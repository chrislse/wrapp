package mq

import (
	"errors"
	"github.com/streadway/amqp"
	"log"
	"time"
)

var amqpURI *string

var conn *amqp.Connection
var ch *amqp.Channel
var replies <-chan amqp.Delivery

type handler func([]byte)

func PerformOnMessages(handlerFunc handler) {
	log.Println("Start consuming the Queue...")
	var count int = 1
	for message := range replies {
		log.Printf("Consuming reply number %d", count)
		handlerFunc(message.Body)
		count++
	}
}

func Publish(body string, routingKey string) {
	log.Printf("publishing %dB body (%s), with routingkey (%s)", len(body), body, routingKey)
	if ch == nil {
		log.Print("connection to rabbitmq might not be ready yet")
	}
	if err := ch.Publish(
		"",         // publish to an exchange
		routingKey, // routing to 0 or more queues
		false,      // mandatory
		false,      // immediate
		amqp.Publishing{
			Headers:      amqp.Table{},
			Body:         []byte(body),
			DeliveryMode: amqp.Transient, // 1=non-persistent, 2=persistent
			Priority:     0,              // 0-9
		},
	); err != nil {
		log.Printf("Exchange Publish: %s", err)
	}
}

func InitAmqp(amqpURI *string) {
	err := errors.New("dummyErrorForInitiatingClientConn")

	Loop(func() {
		err = initConnection(amqpURI, err)
	}, func() bool {
		if err == nil {
			return false
		}

		if err.Error() != "dummyErrorForInitiatingClientConn" {
			log.Printf("cannot establish rabbit connection on get ready to retry. %v", err)
			time.Sleep(5 * time.Second)
		}
		return true
	})
}

func initConnection(amqpURI *string, err error) error {
	var q amqp.Queue

	conn, err = amqp.Dial(*amqpURI)
	if err != nil {
		log.Printf("Failed to connect to RabbitMQ, %s", err)
		return err
	}

	log.Printf("got Connection, getting Channel...")

	ch, err = conn.Channel()
	if err != nil {
		log.Printf("Failed to open a channel, %s", err)
		return err
	}

	log.Printf("declared channel, use default exchange, declaring Queue (%s)", "translated-transactions")

	//The default exchange is a direct exchange with no name (empty string) pre-declared by the broker. It has one special property that makes it very useful for simple applications: every queue that is created is automatically bound to it with a routing key which is the same as the queue name.

	q, err = ch.QueueDeclare(
		"translated-transactions", // name, leave empty to generate a unique name
		true,  // durable
		false, // delete when usused
		false, // exclusive
		false, // noWait
		nil,   // arguments
	)
	if err != nil {
		log.Printf("Error declaring the Queue, %s", err)
		return err
	}

	log.Printf("declared Queue (%q %d messages, %d consumers), binding to Exchange (key %q)",
		q.Name, q.Messages, q.Consumers, "#")

	// differen instances can use its sequence number to show who is consuming, node name
	log.Printf("Queue bound to Exchange, starting Consume (consumer tag %q)", "offer")

	replies, err = ch.Consume(
		q.Name,  // queue
		"offer", // consumer node name
		true,    // auto-ack, //TODO, introduce job queue to reduce the auto-ack
		false,   // exclusive
		false,   // no-local
		false,   // no-wait
		nil,     // args
	)
	if err != nil {
		log.Printf("Error consuming the Queue, %s", err)
		return err
	}

	return nil
}

//TODO move loop into seperate package
type callback_fn func()

type predicate_fn func() bool

func Loop(callback callback_fn, predicate predicate_fn) {
	for predicate() {
		callback()
	}
}
