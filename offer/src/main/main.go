package main

import (
	"flag"
	"fmt"
	"github.com/gocraft/work"
	"github.com/gomodule/redigo/redis"
	"log"

	"http"
	"mq"
	"offer"
	"transaction"
)

var (
	amqpURI       = flag.String("amqp", "amqp://offer:offer@localhost:5672/offer", "AMQP URI")
	fetchDuration = flag.Int("fetch_duration", 10, "Duration to fetch all offers in seconds")
	redisHost     = flag.String("redis_host", "localhost", "Host name of redis store")
	httpPort      = flag.Int("http_port", 8086, "http port")
)

func failOnError(err error, msg string) {
	if err != nil {
		log.Fatalf("%s: %s", msg, err)
		panic(fmt.Sprintf("%s: %s", msg, err))
	}
}

func init() {
	flag.Parse()
	mq.InitAmqp(amqpURI)
}

func main() {
	go http.Serve(*httpPort)
	setupJobQueue()
	mq.PerformOnMessages(handleTransactionOnOffer)
}

var redisPool = &redis.Pool{
	MaxActive: 5,
	MaxIdle:   5,
	Wait:      true,
	Dial: func() (redis.Conn, error) {
		return redis.Dial("tcp", (*redisHost)+":6379")
	},
}

type TransactionContext struct {
	TransactionId int64
}

func setupJobQueue() {
	pool := work.NewWorkerPool(TransactionContext{}, 10, "offer", redisPool)
	pool.Job("transaction", (*TransactionContext).HandleTransactionOnOffer)
	pool.Start()

}

var enqueuer = work.NewEnqueuer("offer", redisPool)

func handleTransactionOnOffer(body []byte) {
	_, err := enqueuer.Enqueue("transaction", work.Q{"body": string(body[:])})
	log.Print("doing enqueue")
	if err != nil {
		log.Fatal(err)
	}
}

func (*TransactionContext) HandleTransactionOnOffer(job *work.Job) error {
	log.Print("handling job")
	body := []byte(job.ArgString("body"))
	t := transaction.ConvertJsonToTransaction(body)
	offer.RunRuleEngine(t)
	return nil
}
