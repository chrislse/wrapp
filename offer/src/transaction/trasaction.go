package transaction

import (
	"encoding/json"
	"log"
)

func ConvertJsonToTransaction(data []byte) (t map[string]interface{}) {
	err := json.Unmarshal(data, &t)
	if err != nil {
		log.Printf("Cannot convert Json to struct %s", err)
	}
	return t
}
