* Todo
- [x] Force fetch all offers
- [x] Add a mq handler to forward offer data
- [x] Move the code base into a job queue design to ensure zero message loss
- [x] retry rabbit connection
- [] Add integration test 
- [] try to come up a case the job task would fail.
