#!/bin/sh

(cd offer; make docker)
(cd offer_store;MIX_ENV=prod GAMECENTER=docker make docker)
(cd integration_test; mix deps; mix compile)
